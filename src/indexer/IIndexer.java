package indexer;

import java.util.LinkedList;

public interface IIndexer {
	void index(String path, LinkedList<String> token);
}
