package indexer;

public class ParserManager {
	
	/*
	public List<String> Parse(String pathToFile){
		String extension = pathToFile.substring(pathToFile.lastIndexOf("."),pathToFile.length());
		IParser parser = FactoryParser.GetParser(extension);
		String content = parser.GetContent(pathToFile);
		return content.split(regex);
	}
	*/
	public String Parse(String pathToFile){
		String extension = pathToFile.substring(pathToFile.lastIndexOf("."),pathToFile.length());
		IParser parser = FactoryParser.GetParser(extension);
		String content = parser.GetContent(pathToFile);
		return content;
	}
}
