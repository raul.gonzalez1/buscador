package indexer;

import java.io.File;
import java.util.LinkedList;

public interface IReader {
	
	/**
	 * Gets a path and returns all document paths within the fullPath
	 * FullPath can be either a folder or a single document. 
	 * @param fullPath
	 * @return list with path documents
	 */
	public LinkedList<File> read(String fullPath); 
}
