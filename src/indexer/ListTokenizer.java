package indexer;

import java.util.LinkedList;
import java.util.StringTokenizer;

public class ListTokenizer implements ITokenizer {

	private LinkedList<String> textList = new LinkedList<String>();
	
	@Override
	public LinkedList<String> tokenize(String docContent) {
		
		StringTokenizer sTokenizer = new StringTokenizer(docContent);
		while (sTokenizer.hasMoreTokens()) {
			String str = sTokenizer.nextToken();
			textList.add(str);
		}
		return textList;
	}

}