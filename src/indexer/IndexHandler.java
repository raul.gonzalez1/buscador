package indexer;

import java.io.File;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class IndexHandler {
	
	ParserManager parserManager;
	IIndexer indexer;
	ITokenizer tokenizer;
	
	public IndexHandler(ParserManager parser, ITokenizer tokenizer ,IIndexer indexer){
		this.parserManager = parser;
		this.indexer = indexer;
		this.tokenizer = tokenizer;
	}
	

	public void index(LinkedList<File> files) {
		//Per file we create a parser depending on the extension using a Factory. We return the data from the file.
		for(File file : files) {
			String docContent = parserManager.Parse(file.getAbsolutePath());
			LinkedList<String> tokens = new LinkedList<String>(tokenizer.tokenize(docContent));
			indexer.index(file.getAbsolutePath(), tokens);
		}	
		Data.TFIDF();
	}
}
