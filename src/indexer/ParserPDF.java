package indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

public class ParserPDF implements IParser{

	@Override
	public String GetContent(String pathToFile) {
		String content = null;
		
	      BodyContentHandler handler = new BodyContentHandler();
	      Metadata metadata = new Metadata();
	      FileInputStream inputstream;
		try {
			inputstream = new FileInputStream(new File(pathToFile));
		    ParseContext pcontext = new ParseContext();
		      
		    PDFParser pdfparser = new PDFParser(); 
		      try {
				pdfparser.parse(inputstream, handler, metadata,pcontext);
			    content = handler.toString();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TikaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Los espacios en PDF son ascii 160. Cambiarlos a espacio normal
		content = content.replace('\u00A0',' ').trim();
		return content;
	}
}