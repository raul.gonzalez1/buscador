package indexer;

public class FactoryParser {

	public static IParser GetParser(String extension) {
		if (extension.contains(".pdf")) {
			return new ParserPDF();
		} else if (extension.contains(".docx")){
			return new ParserDocx();
		} else if (extension.contains(".doc")) {
			return new ParserDoc();
		} else if (extension.contains(".txt")) {
			return new ParserTXT();
		} else if (extension.contains(".xlsx")) {
			return new ParserXlsx();
		}
		return null;
	}
}
