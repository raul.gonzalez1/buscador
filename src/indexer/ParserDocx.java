package indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class ParserDocx implements IParser{

	@Override
	public String GetContent(String pathToFile) {
		
		String content = null;
		try {
			FileInputStream fis = new FileInputStream(new File(pathToFile));
			InputStream is = fis;
			
			XWPFDocument fdocx = new XWPFDocument(is);
			XWPFWordExtractor xWordExtractor = new XWPFWordExtractor(fdocx);
			content = xWordExtractor.getText();
			xWordExtractor.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return content;
	}	

}