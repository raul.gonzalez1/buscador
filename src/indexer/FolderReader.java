package indexer;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;

public class FolderReader implements IReader{

	@Override
	public LinkedList<File> read(String fullPath) {
		File folder = new File(fullPath);
		LinkedList<File> files = new LinkedList<File>(Arrays.asList(folder.listFiles()));
		LinkedList<File> aux =  new LinkedList<File>(files);
		
		for (File file : aux) {
			if (file.isDirectory()){
				files.addAll(read(file.getAbsolutePath()));
			}
		}
		return files;
	}
}