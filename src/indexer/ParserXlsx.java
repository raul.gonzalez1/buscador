package indexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ParserXlsx implements IParser {

	@Override
	public String GetContent(String pathToFile) {
		String content = "";
		try {
			FileInputStream fis = new FileInputStream(new File(pathToFile));
			XSSFWorkbook book = new XSSFWorkbook(fis);
			XSSFSheet sheet = book.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			
			boolean auxBoolean;
			double auxDouble;
			String auxString = null;
			
			while(rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();
	
				while(cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					switch(cell.getCellType()) {
					case BOOLEAN:
						auxBoolean = cell.getBooleanCellValue();
						auxString = String.valueOf(auxBoolean);
						content += auxString + " ";
						break;		
					case NUMERIC:
						auxDouble = cell.getNumericCellValue();
						auxString = String.valueOf(auxDouble);
						content += auxString + " ";
						break;				
					case STRING:
						auxString = cell.getStringCellValue();
						content += auxString +  " " ;
						break; 
					default:
						break;			
					}
				}
			}
			
			book.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}

}
