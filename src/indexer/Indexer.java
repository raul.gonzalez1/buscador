package indexer;

import java.util.Hashtable;
import java.util.LinkedList;

import org.apache.commons.lang3.tuple.Pair;

public class Indexer implements IIndexer {

	@Override
	public void index(String path, LinkedList<String> token) {
		
		Hashtable<String, Integer> hIndex = new Hashtable<String, Integer>();
		
		for (String word : token) {
			if(!(hIndex.contains(word))) {
				hIndex.put(word, 1);
				Data.addHelperTF(word, path);
			}
			else {
				hIndex.put(word, hIndex.get(word) + 1);
			}
		}	
		
		Pair<String, Hashtable<String, Integer>> pair = Pair.of(path, hIndex);
		Data.addTF(pair);
	}
}