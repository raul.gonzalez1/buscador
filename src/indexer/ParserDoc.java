package indexer;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;


public class ParserDoc implements IParser{

	@Override
	public String GetContent(String pathToFile) {
		
		String content = null;

		File file = null;
        WordExtractor extractor = null;
        try
        {

            file = new File(pathToFile);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            HWPFDocument document = new HWPFDocument(fis);
            extractor = new WordExtractor(document);
            content = extractor.getText();
        }
        catch (Exception exep)
        {
            exep.printStackTrace();
        }
		
		return content;
	}

}