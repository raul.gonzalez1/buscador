package indexer;

import java.io.File;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

public class Data implements Serializable{
	
	private static LinkedList<Pair<String,Hashtable<String,Integer>>> TF = new LinkedList<Pair<String,Hashtable<String,Integer>>>();
	private static Hashtable<String, LinkedList<String>> helperTF = new Hashtable<String, LinkedList<String>>();
	private static Hashtable<String, Double> IDF = new Hashtable<String, Double>();
	
	private static Hashtable<String,LinkedList<Pair<String,Double>>> TFIDF = new Hashtable<String,LinkedList<Pair<String,Double>>>();
	
	public static void addTF(Pair<String,Hashtable<String,Integer>> element){
		TF.add(element);	
	}
	
	public static void addHelperTF(String token, String path) {
		if(helperTF.containsKey(token))
			helperTF.get(token).add(path);
		else {
			LinkedList<String> list = new LinkedList<String>();
			list.add(path);
			helperTF.put(token, list);
		}
	}
	
	public static void generateIDF(){
		Set<String> tokens = helperTF.keySet();
        for(String token: tokens){
            Double value = Math.log10(TF.size()/helperTF.get(token).size()) + 1; 
        	IDF.put(token, value);
        }
	}
	public static void addTFIDF(String path, String token, Double IDF){
		if(TFIDF.containsKey(token)){
			Pair<String,Double> pair = Pair.of(path,IDF);
			TFIDF.get(token).add(pair);
	    } else {
			Pair<String,Double> pair = Pair.of(path,IDF);
			LinkedList<Pair<String,Double>> list = new LinkedList<Pair<String,Double>>();
			list.add(pair);
			TFIDF.put(token,list);
	    }
    }
	
	public static void TFIDF(){
		generateIDF();
		for (Pair<String,Hashtable<String,Integer>> pair : TF) {
			String path = pair.getLeft();
			Set<String> tokens = pair.getRight().keySet();
			for (String token : tokens) {
				Double tfidf = pair.getRight().get(token) * IDF.get(token);
				addTFIDF(path,token,tfidf);
			}
		}
    }
	
	public static LinkedList<File> search(String token){
		LinkedList<File> files = new LinkedList<File>();
		for (Pair<String,Double> pair: TFIDF.get(token)) {
			File file = new File(pair.getLeft());
			files.add(file);
		}
		return files;
	}
}
