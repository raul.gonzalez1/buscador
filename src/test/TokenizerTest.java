package test;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;

import org.junit.Test;

import indexer.ListTokenizer;
import indexer.ParserManager;

public class TokenizerTest {

	@Test
	public void TestList() {
		
		ListTokenizer lTokenizer = new ListTokenizer();
		ParserManager TestParser = new ParserManager();
		String pruebaTXT = TestParser.Parse("test/TXTPrueba.txt");
		LinkedList<String> tokenizerTest = new LinkedList<String>();
		
		tokenizerTest = lTokenizer.tokenize(pruebaTXT);
		int tamExpected = 3;
		
		assertEquals(tamExpected, tokenizerTest.size());
	}
	
	@Test
	public void TestListContent() {
		
		ListTokenizer lTokenizer = new ListTokenizer();
		ParserManager TestParser = new ParserManager();
		String pruebaTXT = TestParser.Parse("test/TXTPrueba.txt");
		LinkedList<String> tokenizerTest = new LinkedList<String>();
		
		tokenizerTest = lTokenizer.tokenize(pruebaTXT);		
		String wordExpected = "Habia";
		
		assertEquals(wordExpected, tokenizerTest.getFirst());
	}
}
