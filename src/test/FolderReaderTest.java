package test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import indexer.FolderReader;

public class FolderReaderTest {
	@Test
	public void TestReader() {
		FolderReader reader = new FolderReader();
		
		assertEquals(reader.read("test").size(),5);
	}
}
