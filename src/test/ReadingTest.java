package test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import indexer.ParserManager;

public class ReadingTest {

	@Test
	public void TestPDF() {
		ParserManager TestParser = new ParserManager();
		String pruebaPDF = TestParser.Parse("test/PDFPrueba.pdf");
	
		assertNotNull(pruebaPDF);
	}
	
	@Test
	public void TestPDF2() {
		ParserManager TestParser = new ParserManager();
		String pruebaPDF = TestParser.Parse("test/PDFPrueba.pdf");
		
		assertTrue(pruebaPDF.contains("Habia una vez"));
	}
	
	@Test
	public void TestDoc() {
		ParserManager TestParser = new ParserManager();
		String pruebaDOC = TestParser.Parse("test/DOCPrueba.doc");
		
		assertNotNull(pruebaDOC);
	}
	@Test
	public void TestDoc2() {
		ParserManager TestParser = new ParserManager();
		String pruebaDOC = TestParser.Parse("test/DOCPrueba.doc");
		
		assertTrue(pruebaDOC.contains("Habia una vez"));
	}
	
	@Test
	public void TestDocx() {
		ParserManager TestParser = new ParserManager();
		String pruebaDOCX = TestParser.Parse("test/DOCXPrueba.docx");
		
		assertNotNull(pruebaDOCX);
	}
	@Test
	public void TestDocx2() {
		ParserManager TestParser = new ParserManager();
		String pruebaDOCX = TestParser.Parse("test/DOCXPrueba.docx");
		
		assertTrue(pruebaDOCX.contains("Habia una vez"));
	}
	
	@Test
	public void TestTxt() {
		ParserManager TestParser = new ParserManager();
		String pruebaTXT = TestParser.Parse("test/TXTPrueba.txt");
		
		assertNotNull(pruebaTXT);
	}
	@Test
	public void TestTxt2() {
		ParserManager TestParser = new ParserManager();
		String pruebaTXT = TestParser.Parse("test/TXTPrueba.txt");
		
		assertTrue(pruebaTXT.contains("Habia una vez"));
	}
	@Test
	public void TestXlsx() {
		ParserManager TestParser = new ParserManager();
		String pruebaXLSX = TestParser.Parse("test/XLSXPrueba.xlsx");
		
		assertNotNull(pruebaXLSX);
	}
	@Test
	public void TestXlsx2() {
		ParserManager TestParser = new ParserManager();
		String pruebaXLSX = TestParser.Parse("test/XLSXPrueba.xlsx");
		
		assertTrue(pruebaXLSX.contains("Habia una  vez"));
	}
}
