package frontend;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import indexer.Data;
import indexer.FolderReader;
import indexer.IndexHandler;
import indexer.Indexer;
import indexer.ListTokenizer;
import indexer.ParserManager;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public final class Window extends Application {
	 
	private LinkedList<File> list;
	private LinkedList<File> aux;
	private LinkedList<File> search;
	private VBox indexVBox2 = new VBox();
	private VBox searchVBox = new VBox();
    private Desktop desktop = Desktop.getDesktop();
    
	
    @Override
    public void start(Stage stage) {
    	stage.getIcons().add(new Image("resources/favicon.png"));
        stage.setTitle("JRSearchEngine");
        stage.setWidth(600);
        stage.setHeight(300);
        stage.setMinHeight(300);
        stage.setMinWidth(600);
        
        //Tabs
        TabPane tabPane = new TabPane();
        Tab tabIndex = new Tab("Indexer");
        Tab tabSearch = new Tab("Search");
        Tab tabSettings = new Tab("Settings");
        tabPane.getTabs().add(tabIndex);
        tabPane.getTabs().add(tabSearch);
        tabPane.getTabs().add(tabSettings);
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        Scene scene = new Scene(tabPane);
 
        ///////////////////////////
        //////////INDEXER//////////
        ///////////////////////////
        VBox indexBox = new VBox();
        indexBox.setSpacing(10);
        indexBox.setPadding(new Insets(20,20,20,20));
        indexBox.prefHeightProperty().bind(stage.heightProperty());
        indexBox.prefWidthProperty().bind(stage.widthProperty());
        tabIndex.setContent(indexBox);
        
        //
        HBox indexHBox1 = new HBox();
        indexHBox1.setSpacing(10);
        HBox indexHBox2 = new HBox();
        indexHBox2.setSpacing(10);
        indexBox.getChildren().add(indexHBox1);
        indexBox.getChildren().add(indexHBox2);
        indexHBox1.prefHeightProperty().bind(indexBox.heightProperty().multiply(.75));
        indexHBox1.prefWidthProperty().bind(indexBox.widthProperty());
        indexHBox2.prefHeightProperty().bind(indexBox.heightProperty().multiply(.25));
        indexHBox2.prefWidthProperty().bind(indexBox.widthProperty());
        
        //
        ScrollPane scrollFiles = new ScrollPane();
        scrollFiles.pannableProperty().set(true);
        scrollFiles.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        VBox indexVBox1 = new VBox();
        indexVBox1.setSpacing(10);
        indexHBox1.getChildren().add(scrollFiles);
        indexHBox1.getChildren().add(indexVBox1);
        scrollFiles.prefHeightProperty().bind(indexHBox1.heightProperty());
        scrollFiles.prefWidthProperty().bind(indexHBox1.widthProperty().multiply(.75));
        indexVBox1.prefHeightProperty().bind(indexHBox1.heightProperty());
        indexVBox1.prefWidthProperty().bind(indexHBox1.widthProperty().multiply(.25));
        
        //ScrollPane content in Button action
        indexVBox2.setSpacing(5);
        scrollFiles.setContent(indexVBox2);
        indexVBox2.prefHeightProperty().bind(scrollFiles.heightProperty());
        indexVBox2.prefWidthProperty().bind(scrollFiles.widthProperty());
        
        //Buttons
        Button mchooseBtn = new Button("Add folder");
        mchooseBtn.setTooltip(new Tooltip("Select folder to index"));
        Button chooseBtn = new Button("Add file");
        chooseBtn.setTooltip(new Tooltip("Select files to index"));
        Button removeBtn = new Button("Remove");
        removeBtn.setTooltip(new Tooltip("Remove all selected files"));
        indexVBox1.getChildren().add(mchooseBtn);
        indexVBox1.getChildren().add(chooseBtn);
        indexVBox1.getChildren().add(removeBtn);
        mchooseBtn.prefWidthProperty().bind(indexVBox1.widthProperty());
        chooseBtn.prefWidthProperty().bind(indexVBox1.widthProperty());
        removeBtn.prefWidthProperty().bind(indexVBox1.widthProperty());
        
        //Choose directory button action
        DirectoryChooser directoryChooser = new DirectoryChooser();
        mchooseBtn.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                    	File selectedDirectory = directoryChooser.showDialog(stage);
                    	FolderReader reader = new FolderReader();
                    	if (selectedDirectory != null)
                    		aux = reader.read(selectedDirectory.getAbsolutePath());
                        if (aux != null) {
                            for (File file : aux) {
                            	if (list == null) {
                            		addElement(file);
                            		list = new LinkedList<File>();
                            		list.add(file);
                            	}
                            	else if (!list.contains(file)) {
                            		addElement(file);
                            		list.add(file);
                            	}
                            }
                        }
                    }
                }
            );
        
        //Choose file button action
        FileChooser fileChooser = new FileChooser();
        chooseBtn.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                    	try {
                    		aux = new LinkedList<File>(fileChooser.showOpenMultipleDialog(stage));
						} catch (Exception e2) {
						}
                        if (aux != null) {
                            for (File file : aux) {
                            	if (list == null) {
                            		addElement(file);
                            		list = new LinkedList<File>();
                            		list.add(file);
                            	}
                            	else if (!list.contains(file)) {
                            		addElement(file);
                            		list.add(file);
                            	}
                            }
                        }
                    }
                }
            );
        
        //Remove button action
        removeBtn.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent x) {
                    	indexVBox2.getChildren().clear();
                    	list = null;
                    }
                }
            );
       
        //ProgressBar and Index Button
        ProgressBar progressBar = new ProgressBar(0);
        Button indexBtn = new Button("Index");
        indexBtn.setTooltip(new Tooltip("Index all selected files"));
        indexHBox2.getChildren().add(progressBar);
        indexHBox2.getChildren().add(indexBtn);
        progressBar.prefWidthProperty().bind(indexHBox2.widthProperty().multiply(.75));
        indexBtn.prefHeightProperty().bind(indexHBox2.heightProperty());
        indexBtn.prefWidthProperty().bind(indexHBox2.widthProperty().multiply(.25));
        
        indexBtn.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent x) {
                    	if (list != null) {
	                		ListTokenizer tokenizer = new ListTokenizer();
	                		ParserManager parser = new ParserManager();
	                		Indexer indexer = new Indexer();
	                    	IndexHandler handler = new IndexHandler(parser,tokenizer,indexer);
	                    	handler.index(list);
                    	}
                    }
                }
            );
        
        ///////////////////////////
        ///////////SEARCH//////////
        ///////////////////////////
        VBox searchBox = new VBox();
        searchBox.setSpacing(10);
        searchBox.setPadding(new Insets(20,20,20,20));
        searchBox.prefHeightProperty().bind(stage.heightProperty());
        searchBox.prefWidthProperty().bind(stage.widthProperty());
        tabSearch.setContent(searchBox);
        
        //
        HBox searchHBox1 = new HBox();
        searchHBox1.setSpacing(10);
        ScrollPane results = new ScrollPane();
        searchBox.getChildren().add(searchHBox1);
        searchBox.getChildren().add(results);
        searchHBox1.prefWidthProperty().bind(searchBox.widthProperty());
        results.prefWidthProperty().bind(searchBox.widthProperty());
        results.prefHeightProperty().bind(searchBox.heightProperty());
        
        //SearchBar and Button
        TextField searchBar = new TextField();
        Button searchBtn = new Button("Search");
        searchHBox1.getChildren().add(searchBar);
        searchHBox1.getChildren().add(searchBtn);
        searchBar.prefWidthProperty().bind(searchHBox1.widthProperty().multiply(.75));
        searchBar.prefHeightProperty().bind(searchHBox1.heightProperty());
        searchBtn.prefWidthProperty().bind(searchHBox1.widthProperty().multiply(.25));
        searchBtn.prefHeightProperty().bind(searchHBox1.heightProperty());
        
        //ScrollPane content
        searchVBox.setSpacing(5);
        results.setContent(searchVBox);
        searchVBox.prefHeightProperty().bind(results.heightProperty());
        searchVBox.prefWidthProperty().bind(results.widthProperty());
        
        //Search button action
        searchBtn.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                    	String token = searchBar.getText();
                    	search = Data.search(token);
                    	for (File file : search) {
							addSElement(file);
						}
                    }
                }
            );
        
        ///////////////////////////
        //////////SETTINGS/////////
        ///////////////////////////
        VBox settingsBox = new VBox();
        settingsBox.setSpacing(10);
        settingsBox.setPadding(new Insets(20,20,20,20));
        settingsBox.prefHeightProperty().bind(stage.heightProperty());
        settingsBox.prefWidthProperty().bind(stage.widthProperty());
        tabSettings.setContent(settingsBox);
        
        //Remove button
        Button removeIBtn = new Button("Remove indexation");
        settingsBox.getChildren().add(removeIBtn);
        removeIBtn.setStyle("-fx-base: #CF3017;"
        		+ "-fx-text-fill: white;");
        
        //Remove button action
        removeIBtn.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {
                    	/*
                    	final Stage dialog = new Stage();
                        dialog.initModality(Modality.APPLICATION_MODAL);
                        dialog.initOwner(stage);
                        VBox dialogVbox = new VBox(20);
                        dialogVbox.getChildren().add(new Text("Test"));
                        Scene dialogScene = new Scene(dialogVbox, 300, 200);
                        dialog.setScene(dialogScene);
                        dialog.show();
                        */
                    }
                }
            );
        
        //
        stage.setScene(scene);
        stage.show();
    }
 
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(
                Window.class.getName()).log(
                    Level.SEVERE, null, ex
                );
        }
    }
    
    private void addElement(File file){
    	//Crear lista de objetos
    	HBox fileBox = new HBox();
    	fileBox.prefWidthProperty().bind(indexVBox2.widthProperty());
    	fileBox.setPadding(new Insets(10,10,10,10));
    	fileBox.setSpacing(10);
    	//Image 
    	String input = "/resources/pdf.png";
		Image image = new Image(input);
    	ImageView imageView = new ImageView(image);
    	imageView.setFitHeight(50);
    	imageView.setFitWidth(50);
    	fileBox.getChildren().add(imageView);
    	//Name and action
    	Hyperlink link = new Hyperlink(file.getName());
    	link.setMinSize(150, 10);
    	link.prefWidthProperty().bind(fileBox.widthProperty().multiply(.65));
    	link.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle (ActionEvent e) {
    			openFile(file);
    		}
    	});
    	fileBox.getChildren().add(link);
    	//KB
    	Label labelkb = new Label((int)(file.length()*0.001) + " KB");
    	fileBox.getChildren().add(labelkb);
    	//Add to list
    	indexVBox2.getChildren().add(fileBox);
    }
    
    private void addSElement(File file){
    	//Crear lista de objetos
    	HBox fileBox = new HBox();
    	fileBox.prefWidthProperty().bind(indexVBox2.widthProperty());
    	fileBox.setPadding(new Insets(10,10,10,10));
    	fileBox.setSpacing(10);
    	//Image 
    	String input = "/resources/pdf.png";
		Image image = new Image(input);
    	ImageView imageView = new ImageView(image);
    	imageView.setFitHeight(50);
    	imageView.setFitWidth(50);
    	fileBox.getChildren().add(imageView);
    	//Name and action
    	Hyperlink link = new Hyperlink(file.getName());
    	link.setMinSize(150, 10);
    	link.prefWidthProperty().bind(fileBox.widthProperty().multiply(.65));
    	link.setOnAction(new EventHandler<ActionEvent>() {
    		@Override
    		public void handle (ActionEvent e) {
    			openFile(file);
    		}
    	});
    	fileBox.getChildren().add(link);
    	//KB
    	Label labelkb = new Label((int)(file.length()*0.001) + " KB");
    	fileBox.getChildren().add(labelkb);
    	//Add to list
    	searchVBox.getChildren().add(fileBox);
    }
    
    private String path(String path) {
    	String filePath = new File("").getAbsolutePath();
    	return filePath.concat(path);
	}
}
